WEATHER_PATH = ../jarosova2019patterns-s1
PREREQ_SITES = R/f-sites.R data/sites.csv
CHECKPOINT := R/f-checkpoint.R cache/0-checkpointed

.PHONY = all clean

all: cache/1-counts_interpolated.csv cache/2-threshold-data.csv cache/4-prda.Rdata build/counts.pdf build/threshold.pdf build/prda.pdf build/ml_rmse.csv build/ml_validate.pdf build/ml_explain.pdf

build/ml_explain.pdf: R/7p-ml_explain.R $(CHECKPOINT) cache/7-ml_explain.csv R/f-variable2label.R
	mkdir -p build
	Rscript $<

cache/7-ml_explain.csv: R/7-ml_explain.R $(CHECKPOINT) cache/6-ml_validate.RData cache/gar_fun.r
	Rscript $<

cache/gar_fun.r:
	curl 'https://gist.githubusercontent.com/fawda123/6206737/raw/d6f365c283a8cae23fb20892dc223bc5764d50c7/gar_fun.r' -o $@

build/ml_validate.pdf: R/6p-ml_validate.R $(CHECKPOINT) cache/6-ml_validate.RData R/f-variable2label.R
	mkdir -p build
	Rscript $<

cache/6-ml_validate.RData: R/6-ml_validate.R $(CHECKPOINT) cache/5-ml_rmse.RData
	Rscript $<

build/ml_rmse.csv cache/5-ml_rmse.RData: R/5-ml_rmse.R $(CHECKPOINT) cache/4-ml_train.RData
	Rscript $<

cache/4-ml_train.RData: R/4-ml_train.R $(CHECKPOINT) data/training.csv data/dvsivs.csv cache/3-dataset_annual.csv
	Rscript $<

build/prda.pdf: R/4p-prda.R $(CHECKPOINT) cache/4-prda.csv cache/4-prda.Rdata R/f-variable2label.R
	mkdir -p build
	Rscript $<

cache/4-prda.csv cache/4-prda.Rdata: R/4-prda.R $(CHECKPOINT) cache/3-dataset_annual.csv
	Rscript $<

cache/3-dataset_annual.csv: R/3-dataset_annual.R $(CHECKPOINT) cache/2-counts_annual.csv cache/2-weather_cascades.csv data/dvsivs.csv
	Rscript $<

cache/2-counts_annual.csv: R/2-counts-annual.R $(CHECKPOINT) cache/1-peaks.csv cache/1-counts_interpolated.csv
	Rscript $<

build/threshold.pdf: R/2p-threshold.R $(CHECKPOINT) cache/2-threshold-density.csv cache/2-threshold-data.csv
	mkdir -p build
	Rscript $<

cache/2-threshold-data.csv cache/2-threshold-density.csv: R/2-threshold.R $(CHECKPOINT) cache/1-peaks.csv cache/1-weather.csv R/f-constants.R
	Rscript $<

cache/2-weather_cascades.csv: R/2-weather_cascades.R $(CHECKPOINT) cache/1-weather.csv
	Rscript $<

cache/1-weather.csv: R/1-weather.R $(CHECKPOINT) $(WEATHER_PATH)/weather_grid.csv $(WEATHER_PATH)/weather.csv $(PREREQ_SITES)
	mkdir -p cache
	Rscript $<

cache/1-peaks.csv: R/1-peaks.R $(CHECKPOINT) data/peaks.csv
	mkdir -p cache
	Rscript $<

build/counts.pdf: R/1p-counts_interpolated.R $(CHECKPOINT) cache/1-counts_interpolated.csv
	mkdir -p build
	Rscript $<

cache/1-counts_interpolated.csv: R/1-counts_interpolated.R $(CHECKPOINT) data/counts.csv R/f-constants.R $(PREREQ_SITES)
	mkdir -p cache
	Rscript $<

cache/0-checkpointed: R/0-checkpoint.R
	mkdir -p cache/.checkpoint
	Rscript $<
	touch $@

clean:
	rm -rf build cache
