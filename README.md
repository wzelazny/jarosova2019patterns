# Data and computational scripts for Jarošová et al. 2019 *Patters and Predictions of Barley Yellow Dwarf Virus Vector, Cereal Aphid Migrations in Central Europe*

You can try to reproduce the results by removing the `build` and `cache` directories and running `make`.
In order to do that, additional weather data, available from a separate repository, are needed.
Alternatively, you can inspect the pre-generated files in the `build` and `cache` directories.
