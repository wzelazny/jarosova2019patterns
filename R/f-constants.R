duprectolerance <- 2 # max. tolerated records with the same site and date
dupcounttolerance <- 10 # max. tolerated count discrepancy value
tailtolerance <- 10 # max. tolerated count adjacent to NA count

moving_avg_order <- 5 # order of moving average for temperature smoothing
