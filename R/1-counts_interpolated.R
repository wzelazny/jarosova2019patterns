# Wrangle with aphid counts

source("R/f-checkpoint.R")
library(readr)
suppressPackageStartupMessages(library(dplyr))
suppressPackageStartupMessages(library(lubridate))
library(assertable)
library(tidyr)
library(purrr)
suppressPackageStartupMessages(library(zoo))
suppressPackageStartupMessages(library(magrittr))

source("R/f-constants.R")
source("R/f-sites.R")

interpolate_counts <- function(data) {

   tail_lo <- data %>% select(date) %>% pull %>% min
   tail_hi <- data %>% select(date) %>% pull %>% max
   timeline_small <- crossing(date = seq(tail_lo, tail_hi, by = "day"))
   count_lo <- data %>% filter(date == tail_lo) %>% select(count) %>% pull
   count_hi <- data %>% filter(date == tail_hi) %>% select(count) %>% pull
   timeline_big <- crossing(date = seq(tail_lo %>% floor_date("year"),
                                       tail_hi %>% ceiling_date("year") - 1,
                                       by = "day"))

   data %>%
      # Interpolate between tails
      right_join(timeline_small, by = "date") %>%
      mutate(count = na.approx(count)) %>%
      # Treat tails
      right_join(timeline_big, by = "date") %>%
      {if (count_lo <= tailtolerance) {
         mutate(., count = ifelse(date < tail_lo, 0, count))
      } else {
         .
      }} %>%
      {if (count_hi <= tailtolerance) {
         mutate(., count = ifelse(date > tail_hi, 0, count))
      } else {
         .
      }}
}

counts_interpolated <- read_csv("data/counts.csv",
                   col_types = cols(col_date("%d.%m.%Y"),
                                    col_integer(),
                                    col_integer(),
                                    col_integer())) %>%
   # Identify sites by names
   left_join(sites, by = "site_id") %>%
   select(-site_id) %>%
   select(site, date, everything()) %>%
   arrange(site, date) %>%

   # No count means zero
   mutate(female = ifelse(is.na(female), 0, female),
          male = ifelse(is.na(male), 0, male)) %>%

   # Filter out duplicates and discrepancies
   group_by(site, date) %>%
   filter(n() <= duprectolerance) %>%

   nest %>% # drops grouping
   group_by(site, date) %>%
   mutate(fem_discrep = first(data[[1]]$female) - last(data[[1]]$female) %>%
          abs > dupcounttolerance) %>%
   mutate(mal_discrep = first(data[[1]]$male) - last(data[[1]]$male) %>%
          abs > dupcounttolerance) %>%
   filter(!fem_discrep & !mal_discrep) %>%
   select(-ends_with("_discrep")) %>%
   unnest %>%
   slice(1) %>%
   ungroup %T>%

   # Duplicates gone?
   assert_ids(list(date = unique(.$date), site = unique(.$site)),
              assert_combos = FALSE,
              quiet = TRUE) %>%

   # Ignore sex information
   mutate(count = female + male) %>%
   select(-female, -male) %>%

   # Introduced "interpolated" flag
   mutate(interpolated = FALSE) %>%

   # Interpolate missing days
   mutate(year = year(date) %>% as.integer) %>%
   group_by(site, year) %>%
   nest(.key = "original") %>% # drops grouping
   mutate(interpolated = map(original, ~ interpolate_counts(.x))) %>%
   select(-original) %>%
   unnest %>%
   mutate(interpolated = ifelse(is.na(interpolated), TRUE, FALSE)) %T>%

   # Still no duplicates? No missing days?
   assert_ids(list(date = unique(.$date), site = unique(.$site)),
              quiet = TRUE) %T>%

   write_csv("cache/1-counts_interpolated.csv")
