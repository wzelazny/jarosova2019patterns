# Resolve the threshold hypothesis

source("R/f-checkpoint.R")
library(readr)
suppressPackageStartupMessages(library(dplyr))
library(forecast)
library(magrittr)
suppressPackageStartupMessages(library(tidyr))
library(stringr)
suppressPackageStartupMessages(library(purrr))
source("R/f-constants.R")

weather <- read_csv("cache/1-weather.csv", col_types = "ciiDddddd") %>%
   select(site, date, meantemp) %>%
   # Smooth meantemp using moving average
   group_by(site) %T>%
   {options(warn = -1)} %>%
   mutate(meantemp = ma(meantemp, order = moving_avg_order)) %T>%
   {options(warn = 0)}

threshold <- read_csv("cache/1-peaks.csv", col_types = "ciDiiDDii") %>%
   select(site, year, matches("start[12]_date")) %>%
   gather(variable, date, -site, -year) %>%
   left_join(weather, by = c("site", "date")) %>%
   select(-date) %>%
   mutate(variable = str_replace(variable,
                                 "start([12])_date",
                                 "Tstart\\1_degC")) %T>%
   write_csv("cache/2-threshold-data.csv")

threshold %>%
   select(-year) %>%
   nest(-variable, -site) %>%
   mutate(dens = map(data, ~ .x %>% pull %>% density(na.rm = TRUE))) %>%
   select(-data) %>%
   mutate(dens_coords = map(dens,
                            ~ tibble(meantemp = .x %>% pluck("x"),
                                     value = .x %>% pluck("y")))) %>%
   select(-dens) %>%
   unnest %>%
   write_csv("cache/2-threshold-density.csv")
