source("R/f-checkpoint.R")
suppressPackageStartupMessages(library(dplyr))
library(tidyr)
library(purrr)
suppressPackageStartupMessages(library(magrittr))
library(readr)

load("cache/4-ml_train.RData")
ml_train <- .

rmse_round <- 2

mod_class_order <- tibble(mod_class = c("ann", "ranfor", "mars"),
                          position = 1:3)

ml_train %>%
   select(-training) %>%
   gather(mod_class, model, -peak, -dv, -corcutoff, -dummies, -validation) %>%
   arrange(peak, dv, corcutoff, dummies, mod_class) %>%
   # Select best corcutoffs according to RMSE
   mutate(rmse = map_dbl(model, ~ ifelse(class(.x) == "train",
                                         .x$results$RMSE %>% min(na.rm = TRUE),
                                         NA))) %>%
   filter(!(is.na(rmse))) %>%
   group_by(dv, dummies, mod_class) %>%
   arrange(rmse) %>%
   slice(1) %>%
   ungroup %T>%
   save(file = "cache/5-ml_rmse.RData") %>%
   select(dv, dummies, mod_class, rmse) %>%
   group_by(dv, dummies) %>%
   mutate(rmse = rmse %>% round(rmse_round),
          rmse_mark = ifelse(rmse == min(rmse, na.rm = TRUE),
                             paste0("**", rmse %>% format(nsmall = 2, width = 5), "**"),
                             rmse %>% format(nsmall = 2, width = 5))) %>%
   select(-rmse) %>%
   left_join(mod_class_order, by = "mod_class") %>%
   arrange(position) %>%
   select(-position) %>%
   summarize(rmse_mark_ann_ranfor_mars = rmse_mark %>% paste(collapse = ", ")) %>%
   ungroup %>%
   spread(dummies, rmse_mark_ann_ranfor_mars) %>%
   select(dv, wodummies, widummies) %>%
   write_csv("build/ml_rmse.csv")
